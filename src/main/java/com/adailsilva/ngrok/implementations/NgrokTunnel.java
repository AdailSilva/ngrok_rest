package com.adailsilva.ngrok.implementations;

import java.io.IOException;
import java.util.UUID;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@SuppressWarnings("unused")
public class NgrokTunnel {

	// Run [ngrok start --none] in a valid shell or prompt before running this code.
	// Tunnels Limit Quantity: 4 (four) per session.

	private String url;
	private int port;
	private String name;
	private String ngrokAddr;
	private static String ngrok_address = "http://127.0.0.1:4040";
	private boolean bind_tls = true;

	public NgrokTunnel(String url, int port) throws UnirestException {
		this.name = UUID.randomUUID().toString();
		this.ngrokAddr = url;

		String jsonRequest = String.format("{\"addr\":%d,\"proto\":\"http\",\"name\":\"%s\",\"bind_tls\":%b}",
				this.port, this.name, this.bind_tls);
		
		System.out.println();
		System.out.println("jsonRequest: " + jsonRequest);

		HttpResponse<JsonNode> jsonHttpResponse = Unirest.post(this.ngrokAddr.concat("/api/tunnels"))
				.header("accept", "application/json").header("Content-Type", "application/json; charset=utf8")
				.body(jsonRequest).asJson();

		this.url = jsonHttpResponse.getBody().getObject().getString("public_url");
		
		System.out.println();
		System.out.println("--> URL RESPONSE: " + jsonHttpResponse.getBody().getObject().toString());
		System.out.println();
	}

	public NgrokTunnel(int port) throws UnirestException {
		this(ngrok_address, port);
	}

	public String url() {
		return this.url;
	}

	public void close() throws IOException, UnirestException {
		HttpResponse<String> httpResponse = Unirest.delete(this.ngrokAddr.concat("/api/tunnels/").concat(this.name))
				.asString();
	}

}
