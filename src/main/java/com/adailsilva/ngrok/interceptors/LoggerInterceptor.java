package com.adailsilva.ngrok.interceptors;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.DispatcherType;
// import javax.servlet.Filter;
// import javax.servlet.FilterChain;
// import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
// import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
// import javax.servlet.ServletRequest;
// import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE) // High priority filter.
public class LoggerInterceptor implements HandlerInterceptor {

	// INTERCEPTOR:
	Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());

	@Override
	public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Object object) throws Exception {

		System.out.println();
		System.out.println();
		log.info("Before executing the handler.");

		logDetailsRequest(httpServletRequest);

		System.out.println();
		System.out.println("==========================================================");
		System.out.println("===================== AUTHENTICATION =====================");
		System.out.println("==========================================================");

		// VERIFICATIONS:
		// Checking if the request is a Uplink:
		boolean isUplink = authenticateUplinkRequest(httpServletRequest, httpServletResponse, "01115937359",
				"adail101@hotmail.com", "@Hacker101");

		// Info Request:
		preHandleWithoutObjectHandler(httpServletRequest, httpServletResponse);

		// Attributes with values ​​for comparison:
		String verbHttp = "POST";
		String uri = "/rawbodies/uplinks";

		if (verbHttp.equals(httpServletRequest.getMethod()) && uri.equals(httpServletRequest.getRequestURI())
				&& isUplink) {
			return true;
		} else if (verbHttp.equals(httpServletRequest.getMethod()) && uri.equals(httpServletRequest.getRequestURI())
				&& !isUplink) {
			System.out.println();
			System.err.println("Authentication Error, the request cannot proceed!");
			return false;
		}

		System.out.println();
		System.out.println("=========================================================");
		System.out.println("================= END OF AUTHENTICATION =================");
		System.out.println("=========================================================");

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Object object, ModelAndView modelAndView) throws Exception {

		System.out.println();
		log.info("The execution of the handler is complete.");

	}

	@Override
	public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Object object, Exception exception) throws Exception {

		System.out.println();
		log.info("Request completed.");

	}

	// Verification of the Request type, Searching for the Uplinks:
	public boolean authenticateUplinkRequest(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse, String customHeader, String login, String password)
			throws Exception {

		// Basic Authentication for Uplinks:
		boolean proceed = false;

		String verboHttp = httpServletRequest.getMethod();
		System.out.println("Verb HTTP: " + verboHttp);

		String valorCustomHeader = httpServletRequest.getHeader("AdailSilva");

		System.out.println("Custom header value: " + valorCustomHeader);

		String authorization = httpServletRequest.getHeader("Authorization");
		System.out.println("Authorization: " + authorization);

		if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
			// Authorization = Basic base64credentials
			String base64Credentials = authorization.substring("Basic".length()).trim();
			byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
			String credentials = new String(credDecoded, StandardCharsets.UTF_8);
			// Credentials = username:password
			String[] values = credentials.split(":", 2);

			// Attributes with values ​​for comparison:
			// String customHeader = "01115937359";
			// String login = "adail101@hotmail.com";
			// String password = "@Hacker101";

			// Authentication of attribute values ​​for Uplinks:
			if (customHeader.equals(valorCustomHeader) && login.equals(values[0]) && password.equals(values[1])) {
				proceed = true;
				System.out.println("AUTHENTICATED REQUEST");
				System.out.println("Decoded credentials:");
				System.out.println(credentials);
				System.out.println("Login: " + login);
				System.out.println("Password: " + password);

			} else {
				proceed = false;
				// System.err.println("Authentication Error, the request cannot proceed.");
			}

		}

		return proceed;

	}

	public void preHandleWithoutObjectHandler(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {

		System.out.println("=====================================================");
		System.out.println("====================== REQUEST ======================");
		System.out.println("=====================================================");

		// Method used for REQUEST:
		System.out.println("Type Request: " + httpServletRequest.getMethod());
		System.out.println("Resource URI: " + httpServletRequest.getRequestURI());
		System.out.println("Request URL: " + httpServletRequest.getRequestURL());

		// REQUISITION headings:
		Enumeration<String> headerNames = httpServletRequest.getHeaderNames();

		while (headerNames.hasMoreElements()) {

			String headerName = headerNames.nextElement();
			System.out.println("Header name: " + headerName + ", Value: " + httpServletRequest.getHeader(headerName));
		}

		// REQUISITION Parameters:
		Enumeration<String> params = httpServletRequest.getParameterNames();
		while (params.hasMoreElements()) {

			String paramName = params.nextElement();
			System.out
					.println("Parameter name: " + paramName + ", Value: " + httpServletRequest.getParameter(paramName));

		}

		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("TYPE of Requisition = [" + httpServletRequest.getMethod() + "], ");
		stringBuilder.append("Request URL = [" + httpServletRequest.getRequestURL() + "], ");

		String headersRequest = Collections.list(httpServletRequest.getHeaderNames()).stream()
				.map(headerName -> headerName + " : " + Collections.list(httpServletRequest.getHeaders(headerName)))
				.collect(Collectors.joining(", "));

		if (headersRequest.isEmpty()) {
			stringBuilder.append("Request Header: NONE,");
		} else {
			stringBuilder.append("Request Header: [" + headersRequest + "],");
		}

		String parameters = Collections.list(httpServletRequest.getParameterNames()).stream()
				.map(p -> p + " : " + Arrays.asList(httpServletRequest.getParameterValues(p)))
				.collect(Collectors.joining(",\n"));

		if (parameters.isEmpty()) {
			stringBuilder.append("Request Parameters: NONE.");
		} else {
			stringBuilder.append("Request Parameters: [" + parameters + "].");
		}

		System.out.println("========================================================");
		System.out.println("======================= RESPONSE =======================");
		System.out.println("========================================================");

		logDetailsResponse(httpServletResponse);

		logResponseHttp(httpServletRequest, httpServletResponse);
	}

	// REQUEST (Not used)
	// Simple Log (On the Console):
	public String logRequestHttp(HttpServletRequest request, HttpServletResponse response, Model model)
			throws IOException {

		// Headings:
		Enumeration<String> headerNames = request.getHeaderNames();
		List<String> lHeaderN = new ArrayList<String>();
		String headerNamesForHtml = new String();

		while (headerNames.hasMoreElements()) {
			String HeaderName = (String) headerNames.nextElement();
			lHeaderN.add(HeaderName + " = " + request.getHeader(HeaderName));
			headerNamesForHtml += (String) headerNames.nextElement() + " = " + request.getHeader(HeaderName) + "<br/>";
		}
		model.addAttribute("header", headerNamesForHtml);
		System.out.println(lHeaderN.toString());

		return "Return OK, data on the Console.";
	}

	// RESPONSE:
	// StatusCode and Headers only (in Response):
	public String logResponseHttp(HttpServletRequest request, HttpServletResponse response) {

		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("Response status code = [" + response.getStatus() + "].\n");
		String headersResponse = response.getHeaderNames().stream()
				.map(headerName -> headerName + " : " + response.getHeaders(headerName))
				.collect(Collectors.joining(",\n"));

		if (headersResponse.isEmpty()) {
			stringBuilder.append("None RESPONSE HEADERS,");
		} else {
			stringBuilder.append("RESPONSE HEADERS:\n" + headersResponse + ".");
		}

		System.out.println(stringBuilder.toString());

		return stringBuilder.toString();
	}

	public void logDetailsRequest(HttpServletRequest httpServletRequest) throws Exception {

		System.out.println();
		System.out.println("================ REQUIREMENT DETAILS ================");

		String authType = httpServletRequest.getAuthType();
		System.out.println("AuthType: " + authType);

		String characterEncoding = httpServletRequest.getCharacterEncoding();
		System.out.println("CharacterEncoding: " + characterEncoding);

		String contentType = httpServletRequest.getContentType();
		System.out.println("ContentType: " + contentType);

		String contextPath = httpServletRequest.getContextPath();
		System.out.println("ContextPath: " + contextPath);

		String headerAuthorization = httpServletRequest.getHeader("Authorization");
		System.out.println("Header Authorization value: " + headerAuthorization);

		String headerAdailSilva = httpServletRequest.getHeader("AdailSilva");
		System.out.println("Header AdailSilva Value: " + headerAdailSilva);

		String localAddr = httpServletRequest.getLocalAddr();
		System.out.println("LocalAddr: " + localAddr);

		String localName = httpServletRequest.getLocalName();
		System.out.println("LocalName: " + localName);

		String method = httpServletRequest.getMethod();
		System.out.println("Method: " + method);

		String parameter = httpServletRequest.getParameter(""); // TODO: Procurar por um parâmentro de requisição.
		System.out.println("Parameter: " + parameter);

		String pathInfo = httpServletRequest.getPathInfo();
		System.out.println("PathInfo: " + pathInfo);

		String pathTranslated = httpServletRequest.getPathTranslated();
		System.out.println("PathTranslated: " + pathTranslated);

		String protocol = httpServletRequest.getProtocol();
		System.out.println("Protocol: " + protocol);

		String queryString = httpServletRequest.getQueryString();
		System.out.println("QueryString: " + queryString);

		@SuppressWarnings("deprecation")
		String realPath = httpServletRequest.getRealPath(""); // TODO: Procurar por um path de requisição.
		System.out.println("RealPath: " + realPath);

		String remoteAddr = httpServletRequest.getRemoteAddr();
		System.out.println("RemoteAddr: " + remoteAddr);

		String remoteHost = httpServletRequest.getRemoteHost();
		System.out.println("RemoteHost: " + remoteHost);

		String remoteUser = httpServletRequest.getRemoteUser();
		System.out.println("RemoteUser: " + remoteUser);

		String requestedSessionId = httpServletRequest.getRequestedSessionId();
		System.out.println("RequestedSessionId: " + requestedSessionId);

		String requestURI = httpServletRequest.getRequestURI();
		System.out.println("RequestURI: " + requestURI);

		String scheme = httpServletRequest.getScheme();
		System.out.println("Scheme: " + scheme);

		String serverName = httpServletRequest.getServerName();
		System.out.println("ServerName: " + serverName);

		// String servletPath = httpServletRequest.getServletPath();
		// System.out.println("ServletPath: " + servletPath);

		// AsyncContext asyncContext = httpServletRequest.getAsyncContext();
		// System.out.println("AsyncContext: " + asyncContext);

		Object attribute = httpServletRequest.getAttribute("");
		System.out.println("Attribute: " + attribute);

		Enumeration<String> attributeNames = httpServletRequest.getAttributeNames();
		System.out.println("AttributeNames: " + attributeNames);

		Object _class = httpServletRequest.getClass();
		System.out.println("Class: " + _class);

		int contentLength = httpServletRequest.getContentLength();
		System.out.println("ContentLength: " + contentLength);

		Long contentLengthLong = httpServletRequest.getContentLengthLong();
		System.out.println("ContentLengthLong: " + contentLengthLong);

		Cookie[] cookies = httpServletRequest.getCookies();
		System.out.println("Cookies: " + cookies);

		Long dateHeader = httpServletRequest.getDateHeader(""); // TODO: Encontrar um Header para Ser analisado aqui.
		System.out.println("DateHeader: " + dateHeader);

		DispatcherType dispatcherType = httpServletRequest.getDispatcherType();
		System.out.println("DispatcherType: " + dispatcherType);

		Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
		System.out.println("HeaderNames: " + headerNames);

		Enumeration<String> headers = httpServletRequest.getHeaders(""); // TODO: Encontrar um Header para Ser analisado
																			// aqui.
		System.out.println("Headers: " + headers);

		ServletInputStream inputStream = httpServletRequest.getInputStream();
		System.out.println("InputStream: " + inputStream);

		int intHeader = httpServletRequest.getIntHeader("Facid");
		System.out.println("IntHeader \"Facid\": " + intHeader);

		Locale locale = httpServletRequest.getLocale();
		System.out.println("Locale: " + locale);

		Enumeration<Locale> locales = httpServletRequest.getLocales();
		System.out.println("Locales: " + locales);

		int localPort = httpServletRequest.getLocalPort();
		System.out.println("LocalPort: " + localPort);

		Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
		System.out.println("ParameterMap: " + parameterMap);

		Enumeration<String> parameterNames = httpServletRequest.getParameterNames();
		System.out.println("ParameterNames: " + parameterNames);

		String[] parameterValues = httpServletRequest.getParameterValues(""); // TODO: Encontrar um valores de
																				// parâmetros.
		System.out.println("ParameterValues: " + parameterValues);

		// Part part = httpServletRequest.getPart(""); // TODO: Encontrar um Part.
		// System.out.println("Part: " + part);

		// Collection<Part> parts = httpServletRequest.getParts();
		// System.out.println("Parts: " + parts);

		// BufferedReader bufferedReader = httpServletRequest.getReader();
		// System.out.println("Reader: " + bufferedReader);

		int remotePort = httpServletRequest.getRemotePort();
		System.out.println("RemotePort: " + remotePort);

		RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher(""); // TODO: Encontrar um path.
		System.out.println("RequestDispatcher: " + requestDispatcher);

		StringBuffer requestURL = httpServletRequest.getRequestURL();
		System.out.println("RequestURL: " + requestURL);

		int serverPort = httpServletRequest.getServerPort();
		System.out.println("ServerPort: " + serverPort);

		ServletContext servletContext = httpServletRequest.getServletContext();
		System.out.println("ServletContext: " + servletContext);

		HttpSession session = httpServletRequest.getSession();
		System.out.println("Session: " + session);

		HttpSession booleanSession = httpServletRequest.getSession(true); // TODO: true or false.
		System.out.println("BooleanSession: " + booleanSession);

		Principal userPrincipal = httpServletRequest.getUserPrincipal();
		System.out.println("UserPrincipal: " + userPrincipal);

		System.out.println("============== END OF REQUISITION DETAILS ==============");
		System.out.println();

	}

	public void logDetailsResponse(HttpServletResponse httpServletResponse) throws Exception {

		System.out.println();
		System.out.println("================= RESPONSE DETAILS =================");

		String characterEncoding = httpServletResponse.getCharacterEncoding();
		System.out.println("CharacterEncoding: " + characterEncoding);

		String contentType = httpServletResponse.getContentType();
		System.out.println("ContentType: " + contentType);

		String headerAccessControlAllowOrigin = httpServletResponse.getHeader("Access-Control-Allow-Origin");
		System.out.println("Header Access-Control-Allow-Origin: " + headerAccessControlAllowOrigin);

		String headerAccessControlAllowCredentials = httpServletResponse.getHeader("Access-Control-Allow-Credentials");
		System.out.println("Header Access-Control-Allow-Credentials: " + headerAccessControlAllowCredentials);

		String headerSetCookie = httpServletResponse.getHeader("Set-Cookie");
		System.out.println("Header Set-Cookie: " + headerSetCookie);

		int bufferSize = httpServletResponse.getBufferSize();
		System.out.println("BufferSize: " + bufferSize);

		Class<? extends HttpServletResponse> _class = httpServletResponse.getClass();
		System.out.println("Class: " + _class);

		Collection<String> headerNames = httpServletResponse.getHeaderNames();
		System.out.println("HeaderNames: " + headerNames);

		Collection<String> headers = httpServletResponse.getHeaders("");
		System.out.println("Headers: " + headers);

		Locale locale = httpServletResponse.getLocale();
		System.out.println("Locale: " + locale);

		// ServletOutputStream outputStream = response.getOutputStream();
		// System.out.println("outputStream: " + outputStream);

		int status = httpServletResponse.getStatus();
		System.out.println("Status: " + status);

		// PrintWriter printWriter = response.getWriter();
		// System.out.println("Writer: " + printWriter);

		System.out.println("=============== END DETAILS OF RESPONSE ===============");
		System.out.println();

	}
}
